# -*- coding: utf-8 -*-
from pwn import p64
import struct
import json


class GadgetType:
    JOP = 'jop'
    ROP = 'rop'
    FOP = 'func'


class CompValue:
    def __init__(self, absolute, abs_value=0, base='', offset=0):
        self.absolute = absolute
        self.abs_value = abs_value
        self.base = base
        self.offset = offset

    def get_value(self):
        if self.absolute == 0:
            return {'base': self.base, 'offset': self.offset}
        return self.abs_value

    def to_dict(self):
        if self.absolute:
            value = self.abs_value
        else:
            value = {'base': self.base, 'offset': self.offset}
        return {'absolute': self.absolute, 'value': value}

    @classmethod
    def from_dict(cls, dict_info):
        if 'absolute' not in dict_info or 'value' not in dict_info:
            raise AttributeError

        absolute = dict_info['absolute']
        value = dict_info['value']

        if absolute == 0:
            if 'base' not in value or 'offset' not in value:
                raise AttributeError
            return cls(absolute=absolute, base=value['base'], offset=value['offset'])
        else:
            return cls(absolute=absolute, abs_value=value)



class BaseGadget:
    """
    Basic definition of a gadget.
    """

    def __init__(self, first_inst_address, last_inst, inst_list, size, stack_size, parameters=None, read_regs=None,
                 write_regs=None):
        """
        Initialize a ROP gadget.
        :param first_inst_address: 表示Gadget起始地址
        :param last_inst: 表示该Gadget中最后一条指令的地址信息
        :param inst_list: 表示Gadget中指令列表
        :param size: 表示Gadget代码的字节长度
        :param stack_size: 表示Gadget占用栈空间长度
        :param parameters: 用于为不同Gadget传递参数
        :param read_regs: 该Gadget读取的寄存器列表
        :param write_regs: 该Gadget写入的寄存器列表
        """
        if parameters is None:
            parameters = []
        if read_regs is None:
            read_regs = []
        if write_regs is None:
            write_regs = []
        self.__first_inst_address = first_inst_address
        self.__last_inst = last_inst
        self.__inst_list = inst_list
        self.__size = size
        self.__stack_size = stack_size
        self.__parameters = parameters
        self.__read_regs = read_regs
        self.__write_regs = write_regs
        self.gadget_type = GadgetType.ROP  # default to ROP
        self.jmp_target_reg = ""  # only for JOP

    @classmethod
    def from_dict(cls, dict_info):
        if not all(k in dict_info for k in ('first_inst_addr', 'last_inst', 'inst_list', 'size', 'stack_size')):
            raise AttributeError

        if 'parameters' in dict_info.keys():
            parameters = dict_info['parameters']
        else:
            parameters = None

        if 'read_regs' in dict_info.keys():
            read_regs = dict_info['read_regs']
        else:
            read_regs = None

        if 'write_regs' in dict_info.keys():
            write_regs = dict_info['write_regs']
        else:
            write_regs = None

        _obj = cls(first_inst_address=dict_info['first_inst_addr'],
                   last_inst=CompValue.from_dict(dict_info['last_inst']),
                   inst_list=dict_info['inst_list'], size=dict_info['size'],
                   stack_size=dict_info['stack_size'], parameters=parameters,
                   read_regs=read_regs, write_regs=write_regs)

        if 'gadget_type' in dict_info.keys():
            gadget_type = dict_info['gadget_type']
        else:
            gadget_type = GadgetType.ROP

        if gadget_type == GadgetType.JOP:
            if 'jmp_target_reg' in dict_info.keys():
                jmp_target_reg = dict_info['jmp_target_reg']
            else:
                jmp_target_reg = "ambiguous_reg"
            _obj.gadget_type = GadgetType.JOP
            _obj.jmp_target_reg = jmp_target_reg
        else:
            _obj.gadget_type = GadgetType.ROP
        return _obj

    def __str__(self):
        inst_str = ' \n'.join(self.__inst_list)
        return inst_str

    @property
    def size(self):
        return self.__size

    @property
    def stack_size(self):
        return self.__stack_size

    @property
    def inst_count(self):
        return len(self.__inst_list)

    @property
    def address(self):
        return self.__first_inst_address

    @property
    def last_inst_addr(self):
        return self.__last_inst

    @property
    def affected_regs(self):
        return self.__write_regs

    @property
    def raw_data(self):
        data = b''
        data += p64(self.__first_inst_address)
        if self.__parameters is None:
            return data
        for param in self.__parameters:
            if isinstance(param, int):
                data += p64(param)
            else:
                data += p64(struct.unpack('>Q', param)[0])
        if len(data) > self.__stack_size:
            return data[:self.__stack_size]
        return data

    def to_dict(self):
        new_params = []
        for param in self.__parameters:
            if isinstance(param, int):
                new_params.append(param)
            else:
                new_params.append(struct.unpack('>Q', param)[0])

        info = {'first_inst_addr': self.__first_inst_address, 'last_inst': self.__last_inst.to_dict(),
                'inst_list': self.__inst_list, 'size': self.__size,
                'stack_size': self.__stack_size, 'parameters': new_params,
                'read_regs': self.__read_regs, 'write_regs': self.__write_regs,
                'type': self.gadget_type}
        if self.gadget_type == GadgetType.JOP:
            info['jmp_target_reg'] = self.jmp_target_reg
        return info

    def add_parameter(self, param):
        self.__parameters.append(param)

    def to_json(self):
        return json.dumps(self.to_dict())


class GadgetChain:
    def __init__(self):
        self.__gadget_list = []

    @classmethod
    def from_json(cls, json_data):
        data_info = json.loads(json_data)
        obj = cls()
        for data in data_info:
            obj.__gadget_list.append(BaseGadget.from_dict(data))
        return obj

    def __str__(self):
        chain_str = ''
        for gadget in self.__gadget_list:
            chain_str += str(gadget)
            chain_str += '\n'
        return chain_str

    @property
    def inst_count(self):
        count = 0
        for gadget in self.__gadget_list:
            count += gadget.inst_count
        return count

    @property
    def stack_size(self):
        size = 0
        for gadget in self.__gadget_list:
            size += gadget.stack_size
        return size

    @property
    def raw_data(self):
        data = b''
        for gadget in self.__gadget_list:
            data += gadget.raw_data
        return data

    @property
    def gadget_list(self):
        return self.__gadget_list

    def add_gadget(self, gadget):
        self.__gadget_list.append(gadget)

    def to_json(self):
        data = []
        for gadget in self.__gadget_list:
            data.append(gadget.to_dict())
        return json.dumps(data)
