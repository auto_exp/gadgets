# -*- coding: utf-8 -*-
from pwn import p64
import struct
import json
from gadgets.gadget_base import *


class JOPGadget(BaseGadget):
    """
    Basic definition of a JOP gadget.
    """

    def __init__(self, first_inst_address, last_inst, inst_list, size, stack_size, jmp_target_reg='', parameters=None,
                 read_regs=None,
                 write_regs=None):
        """
        Initialize a ROP gadget.
        :param first_inst_address: 表示Gadget起始地址
        :param last_inst: 表示该Gadget中最后一条指令的地址信息
        :param inst_list: 表示Gadget中指令列表
        :param size: 表示Gadget代码的字节长度
        :param stack_size: 表示Gadget占用栈空间长度
        :param jmp_target_reg: 最后一条指令跳转的目标寄存器
        :param parameters: 用于为不同Gadget传递参数
        :param read_regs: 该Gadget读取的寄存器列表
        :param write_regs: 该Gadget写入的寄存器列表
        """
        super().__init__(first_inst_address, last_inst, inst_list, size, stack_size, parameters, read_regs, write_regs)
        self.gadget_type = GadgetType.JOP
        self.jmp_target_reg = jmp_target_reg
